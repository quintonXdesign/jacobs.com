jQuery(document).ready(function($){
	var contentSections = $('.cd-section'),
		navigationItems = $('#cd-vertical-nav a');

	updateNavigation();
	$(window).on('scroll', function(){
		updateNavigation();
	});

	//smooth scroll to the section
	navigationItems.on('click', function(event){
        event.preventDefault();
        smoothScroll($(this.hash));
    });
    //smooth scroll to second section
    $('.cd-scroll-down').on('click', function(event){
        event.preventDefault();
        smoothScroll($(this.hash));
    });

    //open-close navigation on touch devices
    $('.touch .cd-nav-trigger').on('click', function(){
    	$('.touch #cd-vertical-nav').toggleClass('open');
  
    });
    //close navigation on touch devices when selectin an elemnt from the list
    $('.touch #cd-vertical-nav a').on('click', function(){
    	$('.touch #cd-vertical-nav').removeClass('open');
    });

	function updateNavigation() {
		contentSections.each(function(){
			$this = $(this);
			var activeSection = $('#cd-vertical-nav a[href="#'+$this.attr('id')+'"]').data('number') - 1;
			if ( ( $this.offset().top - $(window).height()/2 < $(window).scrollTop() ) && ( $this.offset().top + $this.height() - $(window).height()/2 > $(window).scrollTop() ) ) {
				navigationItems.eq(activeSection).addClass('is-selected');
			}else {
				navigationItems.eq(activeSection).removeClass('is-selected');
			}
		});
	}

	function smoothScroll(target) {
        $('body,html').animate(
        	{'scrollTop':target.offset().top},
        	600
        );
	}


	//$('.mainNav .level2').wrap('<div class="drop"></div>');

	// divide submenu of each "Learn About Us" menu item into columns
	$(".mainNav .level1 > li:first-child + li .level2 > li").each(function () {
	    var divsSec = $(this).find('.level3 > li');
	    var colLength = ($(this).children('a').text() == 'Industries') ? 5 : 4;
	    for (var i = 0; i < divsSec.length; i += colLength) {
	        divsSec.slice(i, i + colLength).wrapAll("<li class='new2'><ul></ul></li>");
	    }
	});
    


	$('.mainNav .level2 li.has-children a').hover(function () {
	    $('.mainNav .level2 li.has-children a').addClass('unhovered');
	    $(this).removeClass('unhovered');
	}, function () {
	    $('.mainNav .level2 li.has-children a').removeClass('unhovered');
	});

    /* home-sticky-nav */
	var $menuButton = $("#cbp-spmenu-s4");
	var $showBottom = $("#showBottom");
	var $menuIcon = $("#menuIcon");
	var $menuButtonClicked = 0;

	$showBottom.on("click", function () {
	    $menuButtonClicked = $menuButtonClicked++;
	    $(this).toggleClass("active");
	    $menuButton.toggleClass("cbp-spmenu-open");
	    $menuIcon.toggleClass("ti-angle-down").toggleClass("ti-angle-up");
	});
    
    //auto close the box on homepage
	setTimeout(function () {
	    if ($menuButtonClicked <= 1) {
	        $showBottom.trigger("click");
	    }
	}, 15000);
    

	$(".tabs li").click(function () {
	    if (!$showBottom.hasClass("active")) {
            $showBottom.trigger("click");
	    }
	});

    //open menu on initial load
	$showBottom.delay(5000).trigger("click");
    /*end home-sticky-nav */

    /* sticky-nav */
	var $pageMenuButton = $(".cbp-spmenu-s1");
	var $showLeft = $("#showLeft");

	$showLeft.on("click", function () {
	    $(this).toggleClass("active");
	    $pageMenuButton.toggleClass("cbp-spmenu-open");
	});

	$showLeft.trigger("click");
    /*end sticky-nav */


});