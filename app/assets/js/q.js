


function accordionDiagSlider(){
    $(".accordion-trigger").on("click", function(){
        console.log("accordion-trigger");
        $(".as-panels").removeClass('through');    

        $('#example3').accordionSlider('openPanel', 0);

    });


    var $accSliderHeight = 0
    jQuery(document).ready(function($) {
        $windowHeight = $(window).height();
        $cotmNavBarHeight = $('.careers-on-the-move .nav-bar').outerHeight();
        $headHeight = $('header .nav-bar').outerHeight(); 
        $footHeight = $('.main-content footer').outerHeight();
        $accSliderHeight = $windowHeight - $headHeight - $footHeight - $cotmNavBarHeight;

        // instantiate the accordion
        $('#example3').accordionSlider({
            width: '100%',
            height: 700,
            responsiveMode: 'custom',
            closePanelsOnMouseOut: false,
            shadow: true,
            panelDistance: 1,
            maxOpenedPanelSize: '60%',
            closePanelsOnMouseOut: false,
            //startPanel: 2,
            //openPanel: 2,
            autoplay: false,
            mouseWheel: false,

            breakpoints: {
                960: {visiblePanels: 5},
                800: {visiblePanels: 3, orientation: 'vertical', width: 600, height: 500},
                650: {visiblePanels: 4},
                500: {visiblePanels: 3, orientation: 'vertical', aspectRatio: 1.2}
            }
        });


        $('.as-left-trigger').click(function(e){
            e.preventDefault();
            $('#example3').accordionSlider('previousPanel');
        });
        $('.as-right-trigger').click(function(e){
            e.preventDefault();
            console.log('next click test');
            $('#example3').accordionSlider('nextPanel');
        });
    });

    // setTimeout(function(){
    //         // $('#example3').accordionSlider('openPanel', 0);
    //         $("#example3 .as-panel:first-of-type").trigger('click');
    //         console.log('over');
    //     }, 1500);
    var $outsideSliderHeight = $(window).height();
    // setTimeout(function(){
    //     $('#example3').accordionSlider('openPanel', 0);
    // }, 1500);
}
accordionDiagSlider();

/*********************************/
/* Vertical Icons Slick Carousel */
/*********************************/
function verticalCarousel(){
    var $vertIconCarousel = $(".vert-icon-carousel");

    if($(window).width() > 990){
        $(".vert-icon-nav.mobile-hide").addClass('use-me');
    } else {
        $(".vert-icon-nav.desktop-hide").addClass('use-me');
    }

    $(document).ready(function(){
        $vertIconCarousel.slick({
            infinite: true,
            arrows: false,
            // autoplaySpeed: 4000,
            // autoplay: true
        });
    });

    $vertIconCarousel.on('init', function(){
        $(".svg-holder").eq(0).addClass('on');
    });

    $vertIconCarousel.on('afterChange', function(){
        curIndex = $vertIconCarousel.slick('slickCurrentSlide');    //get slide id
        $(".svg-holder").removeClass('on');
        var navIndex = $(".vert-icon-nav.use-me li").eq(curIndex).children('.svg-holder').addClass('on');
    });

    $(".vert-icon-nav.use-me a").click(function(e){
        e.preventDefault();
        slideIndex = $(this).parent().index();
        $vertIconCarousel.slick('slickGoTo', parseInt(slideIndex) );
    });

    $("#vert-icon-prev").click(function(e){
        e.preventDefault();
        $vertIconCarousel.slick("slickPrev");
    });

    $("#vert-icon-next").click(function(e){
        e.preventDefault();
        $vertIconCarousel.slick("slickNext");
    });
}
verticalCarousel();

/*********************************/
/* Tabbed Icons Slick Carousel */
/*********************************/
function tabbedCarousel(){
    var $tabbedIconCarousel = $(".tabbed-icon-carousel");

    if($(window).width() > 990){
        
        $(".tabbed-icon-nav.mobile-hide").addClass('use-me');
    } else {
        $(".tabbed-icon-nav.desktop-hide").addClass('use-me');
    }

    $(document).ready(function(){
        $tabbedIconCarousel.slick({
            infinite: true,
            arrows: false,
            // autoplaySpeed: 4000
            // fase: true,

        });
    });

    $tabbedIconCarousel.on('init', function(){
        $(".svg-holder").eq(0).addClass('on');
    });

    $tabbedIconCarousel.on('afterChange', function(){
        curIndex = $tabbedIconCarousel.slick('slickCurrentSlide');    //get slide id
        $(".svg-holder").removeClass('on');
        var navIndex = $(".tabbed-icon-nav.use-me li").eq(curIndex).children('.svg-holder').addClass('on');
    });

    $(".tabbed-icon-nav.use-me a").click(function(e){
        // console.log('clicked use-me');
        e.preventDefault();
        slideIndex = $(this).parent().index();
        $tabbedIconCarousel.slick('slickGoTo', parseInt(slideIndex) );
        // console.log('slideindex' + slideIndex);
    });

    $("#tabbed-icon-prev").click(function(e){
        e.preventDefault();
        $tabbedIconCarousel.slick("slickPrev");
    });

    $("#tabbed-icon-next").click(function(e){
        e.preventDefault();
        $tabbedIconCarousel.slick("slickNext");
    });
}
tabbedCarousel();

/*********************************/
/* Horizontal icon Slick Carousel */
/*********************************/
function horizontalIconCarousel() {
    var $horizontalIconCarousel = $(".horizontal-icon-carousel");
    var $horizontalIconCarousel2 = $(".horizontal-icon-carousel2");

    if($(window).width() > 990){
        // console.log('hey');
        $(".horizontal-icon-nav.mobile-hide").addClass('use-me');
    } else {
        $(".numbered-nav.desktop-hide").addClass('use-me');
    }

    $(document).ready(function(){
        $horizontalIconCarousel.slick({
            arrows: false,
            asNavFor: '.horizontal-icon-carousel2',
            focusOnSelect: true
        });
        $horizontalIconCarousel2.slick({
            arrows: false,
            slidesToShow: 3,
            asNavFor: '.horizontal-icon-carousel',
            focusOnSelect: true
        });
    });

    $horizontalIconCarousel.on('init', function(){
        $(".svg-holder").eq(0).addClass('on');
    });

    $horizontalIconCarousel.on('afterChange', function(){
        curIndex = $horizontalIconCarousel.slick('slickCurrentSlide');    //get slide id
        $horizontalIconCarousel2.slick('slickGoTo', parseInt(curIndex) );
        $(".horizontal-icon-nav.use-me a, .numbered-nav a").removeClass('on');
        var navIndex = $(".horizontal-icon-nav.use-me li, .numbered-nav li").eq(curIndex).children('.svg-holder').addClass('on');
    });

    $(".horizontal-icon-nav.use-me a, .numbered-nav a").click(function(e){
        e.preventDefault();
        slideIndex = $(this).parent().index();
        $horizontalIconCarousel.slick('slickGoTo', parseInt(slideIndex) );
    });

    $("#horizontal-icon-prev").click(function(e){
        e.preventDefault();
        $horizontalIconCarousel.slick("slickPrev");
    });

    $("#horizontal-icon-next").click(function(e){
        e.preventDefault();
        $horizontalIconCarousel.slick("slickNext");
    });
}
horizontalIconCarousel();

/*********************************/
/* Horizontal Pic Slick Carousel */
/*********************************/
function horizontalCarousel() {
    var $horizontalPicCarousel = $(".horizontal-pic-carousel");

    if($(window).width() > 990){
        
        $(".horizontal-pic-nav.mobile-hide").addClass('use-me');
    } else {
        $(".numbered-nav.desktop-hide").addClass('use-me');
    }

    $(document).ready(function(){
        $horizontalPicCarousel.slick({
            infinite: true,
            arrows: false,
            // autoplaySpeed: 4000,
            // fase: true,
            // autoplay:true
        });
    });

    $horizontalPicCarousel.on('init', function(){
        $(".svg-holder").eq(0).addClass('on');
    });

    $horizontalPicCarousel.on('afterChange', function(){
        curIndex = $horizontalPicCarousel.slick('slickCurrentSlide');    //get slide id
        $(".svg-holder").removeClass('on');
        var navIndex = $(".horizontal-pic-nav.use-me li, .numbered-nav li").eq(curIndex).children('.svg-holder').addClass('on');
    });

    $(".horizontal-pic-nav a, .numbered-nav a").click(function(e){
        e.preventDefault();
        slideIndex = $(this).parent().index();
        $horizontalPicCarousel.slick('slickGoTo', parseInt(slideIndex) );
    });

    $("#horizontal-pic-prev").click(function(e){
        e.preventDefault();
        $horizontalPicCarousel.slick("slickPrev");
    });

    $("#horizontal-pic-next").click(function(e){
        e.preventDefault();
        $horizontalPicCarousel.slick("slickNext");
    });
}
horizontalCarousel();



/*********************************/
/* Landing page hovers */
/*********************************/
var $landingSlide = $(".landing-slide");
var $careerBtn = $(".career-btn");

$landingSlide.mouseenter(function(e) {
    var careerPopUp = $(this).children(".career-btn");
    TweenLite.to(careerPopUp, .5, {autoAlpha:1, y: -15});
});
$landingSlide.mouseleave(function(e) {
    var careerPopUp = $(this).children(".career-btn");
    TweenLite.to(careerPopUp, .5, {autoAlpha:0, y: 0});
});

var tempScrollTop = $(window).scrollTop();
$(window).scrollTop(tempScrollTop);
$('.career-btn a').on('click', function(e){
    e.preventDefault();
    // console.log('before load');
    TweenLite.to('.careers-on-the-move .section-content', .5, {opacity: 1});
    var url = $(this).attr('href');
    
    // LOADING EXTERNAL PAGES
    $( ".careers-on-the-move .section-content" ).load( url + " .careers-wrapper", function() {
      // alert( "Load was performed." );
        $(window).scrollTop(tempScrollTop);
        // console.log("test");
        accordionDiagSlider();
        console.log("test acc");
        viewCareer();
        tabbedCarousel();
        horizontalCarousel();
        horizontalIconCarousel();
        verticalCarousel();
        mapCarousel();
        // console.log('loaded');


        // window.history.pushState('','', url);

        // var slides = $('.landing-slide');
        // // $.each(slides, function(index, slide){
        // //     console.log(slide);
        // // }); 
        TweenLite.to('.careers-on-the-move .section-content', .5, {opacity: 1});  
    });
// }, 5000);
});

/**********************/
/* Page Transition */
/**********************/
function viewCareer(){
    // $(".as-panels").removeClass('through');

    var $viewCareer = $(".view-career"),
        $backToBio = $(".back-to-bio"),
        tl = new TimelineLite();

    $viewCareer.on('click', function(e){
        e.preventDefault();
        console.log('Career clicked');
        $curPage = $(this).closest(".careers-page");
        $nextPage = $(this).closest(".careers-page").next(".careers-page");
        // tl.play();
        TweenLite.set($nextPage, {autoAlpha:1, xPercent: 100});
        TweenLite.to($curPage, 0.5, {xPercent:-100, autoAlpha:0})
        TweenLite.to($nextPage, 0.5, {autoAlpha:1, xPercent:0});
    });
    $backToBio.on('click', function(e){
        e.preventDefault();
        $curPage = $(this).closest(".careers-page");
        $prevPage = $(this).closest(".careers-page").prev(".careers-page");
        TweenLite.set($prevPage, {autoAlpha:1, xPercent: -100});
        TweenLite.to($curPage, 0.5, {xPercent:100, autoAlpha:0})
        TweenLite.to($prevPage, 0.5, {autoAlpha:1, xPercent:0});
    });
}
viewCareer();


/**********************/
/* TweenMax Map Carousel */
/**********************/

function mapCarousel(){

    if(!$('.careers-on-the-move').hasClass('map-careers')){
        initMap();    
    }
    

    var $mapCarousel = $(".map-slide-wrapper");

    if($(window).width() > 990){
        // console.log('hey');
        $(".horizontal-pic-nav.mobile-hide").addClass('use-me');
    } else {
        $(".numbered-nav.desktop-hide").addClass('use-me');
    }

    $(document).ready(function(){
        $mapCarousel.slick({
            // infinite: true,
            arrows: false,
            // autoplaySpeed: 4000,
            // fase: true,
            // autoplay: true
        });
    });

    $mapCarousel.on('init', function(){
        // $mapCarousel.slick('slickCurrentSlide')
    });

    $mapCarousel.on('afterChange', function(){
        var slides = $(".map-slide");
        curIndex = $mapCarousel.slick('slickCurrentSlide');

        console.log();
        var zoomNum = map.getZoom();
        var curMapSlide = $(".map-slide");
        var mapSlideNum = $mapCarousel.slick('slickCurrentSlide');
        var newZoom = parseInt(curMapSlide[mapSlideNum].dataset.zoom);

        $mapCarousel.slick("slickNext");
        map.setZoom(newZoom);

        $(".svg-holder").removeClass('on');
        $(".numbered-nav a").removeClass('on');
        var navIndex = $(".numbered-nav li").eq(curIndex).children('.svg-holder').addClass('on');
    });

    $(".numbered-nav.use-me a").click(function(e){
        e.preventDefault();
        slideIndex = $(this).parent().index();
        $mapCarousel.slick('slickGoTo', parseInt(slideIndex) );
    });

    $(".map-carousel-prev").click(function(e){
        console.log('prev clicked');
        var zoomNum = map.getZoom();
        var curMapSlide = $(".map-slide");
        var mapSlideNum = $mapCarousel.slick('slickCurrentSlide');
        
        e.preventDefault();
        $mapCarousel.slick("slickPrev");
        console.log(map);
        var curMapSlide = $(".map-slide");
        var slideBG = curMapSlide[mapSlideNum].getAttribute('data-bg')
        var sectionBG = curMapSlide[mapSlideNum].closest('.map-parent');
        for (var i = 0; i < sectionBG.childNodes.length; i++) {
            if (sectionBG.childNodes[i].className == "map-slide-bg") {
              sectionBG.childNodes[i].style.backgroundImage = "url(" + slideBG + ")";
              break;
            }        
        }

        var newZoom = parseInt(curMapSlide[mapSlideNum].dataset.zoom);
        map.setZoom(newZoom);
    });


    $(".map-carousel-next").click(function(e){
        var zoomNum = map.getZoom();
        var curMapSlide = $(".map-slide");
        var mapSlideNum = $mapCarousel.slick('slickCurrentSlide');

        
        e.preventDefault();
        $mapCarousel.slick("slickNext");
        console.log('hey');
        // console.log('curMapSlide[mapSlideNum]', curMapSlide[mapSlideNum]);
        console.log('curMapSlide[mapSlideNum].closest(".map-parent")', curMapSlide[mapSlideNum].closest('.map-parent'));
        var slideBG = curMapSlide[mapSlideNum].getAttribute('data-bg');
        var sectionBG = curMapSlide[mapSlideNum].closest('.map-parent');
        console.log('hey');
        console.log(sectionBG.childNodes);
        for (var i = 0; i < sectionBG.childNodes.length; i++) {
            if (sectionBG.childNodes[i].className == "map-slide-bg") {
                console.log('hey jey');

              sectionBG.childNodes[i].style.backgroundImage = "url(" + slideBG + ")";
              break;
            }        
        }
        var newZoom = parseInt(curMapSlide[mapSlideNum].dataset.zoom);
        map.setZoom(newZoom);
    });
}
mapCarousel();

/**********************/
/*SVG Object Inner CSS*/
/**********************/
$(".svg-holder").hover(function(){
    var $this = $(this).find('the-icon')
    console.log($this);
});


$(".peter-image img").click(function(event) {
    /* Act on the event */
    console.log('click');
    // initMap();
});

/**********************/
/*Google Map*/
/**********************/

var map;
function initMap() {
  
  map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 52.160114, lng: 4.497010},
    zoom: 8,
    styles: [
      {
              "featureType": "administrative",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": "-100"
                  }
              ]
          },
          {
              "featureType": "administrative.province",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "off"
                  }
              ]
          },
          {
              "featureType": "landscape",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": -100
                  },
                  {
                      "lightness": 65
                  },
                  {
                      "visibility": "on"
                  }
              ]
          },
          {
              "featureType": "poi",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": -100
                  },
                  {
                      "lightness": "50"
                  },
                  {
                      "visibility": "simplified"
                  }
              ]
          },
          {
              "featureType": "road",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": "-100"
                  }
              ]
          },
          {
              "featureType": "road.highway",
              "elementType": "all",
              "stylers": [
                  {
                      "visibility": "simplified"
                  }
              ]
          },
          {
              "featureType": "road.arterial",
              "elementType": "all",
              "stylers": [
                  {
                      "lightness": "30"
                  }
              ]
          },
          {
              "featureType": "road.local",
              "elementType": "all",
              "stylers": [
                  {
                      "lightness": "40"
                  }
              ]
          },
          {
              "featureType": "transit",
              "elementType": "all",
              "stylers": [
                  {
                      "saturation": -100
                  },
                  {
                      "visibility": "simplified"
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                  {
                      "hue": "#ffff00"
                  },
                  {
                      "lightness": -25
                  },
                  {
                      "saturation": -97
                  }
              ]
          },
          {
              "featureType": "water",
              "elementType": "labels",
              "stylers": [
                  {
                      "lightness": -25
                  },
                  {
                      "saturation": -100
                  }
              ]
          }
    ]
  });

}
